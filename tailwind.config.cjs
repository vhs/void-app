const plugin = require('tailwindcss/plugin');
const { fontFamily } = require('tailwindcss/defaultTheme');

module.exports = {
	content: ['./src/**/*.{html,svelte}'],
	theme: {
		fontFamily: {
			sans: ['Lato', ...fontFamily.sans],
			serif: ['Lustria', ...fontFamily.serif],
			mono: ['"Fira Mono"', ...fontFamily.mono]
		},
		extend: {}
	},
	plugins: [
		require('@tailwindcss/forms'),
		require('@tailwindcss/typography'),
		/**
		 * Firefox plugin for Tailwind CSS. Add styles that target Firefox browser only.
		 * https://gist.github.com/samselikoff/b3c5126ee4f4e69e60b0af0aa5bfb2e7
		 */
		plugin(function ({ addVariant, e, postcss }) {
			addVariant('firefox', ({ container, separator }) => {
				const isFirefoxRule = postcss.atRule({
					name: '-moz-document',
					params: 'url-prefix()'
				});
				isFirefoxRule.append(container.nodes);
				container.append(isFirefoxRule);
				isFirefoxRule.walkRules((rule) => {
					rule.selector = `.${e(`firefox${separator}${rule.selector.slice(1)}`)}`;
				});
			});
		})
	]
};
