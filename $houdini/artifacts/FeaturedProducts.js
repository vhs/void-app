export default {
    name: "FeaturedProducts",
    kind: "HoudiniQuery",
    hash: "0d96b9bb845d96b6c1b35d2c60316979de83693bf64945e567008cadeab66b8a",

    raw: `query FeaturedProducts {
  products(first: 24) {
    edges {
      node {
        id
        ...ProductCard
      }
    }
  }
}

fragment ProductCard on Product {
  name
  description
  category {
    slug
    name
    id
  }
  minimalVariantPrice {
    currency
    amount
  }
  slug
  images {
    id
    alt
    url
  }
}
`,

    rootType: "Query",

    selection: {
        products: {
            type: "ProductCountableConnection",
            keyRaw: "products(first: 24)",

            fields: {
                edges: {
                    type: "ProductCountableEdge",
                    keyRaw: "edges",

                    fields: {
                        node: {
                            type: "Product",
                            keyRaw: "node",

                            fields: {
                                id: {
                                    type: "ID",
                                    keyRaw: "id"
                                },

                                name: {
                                    type: "String",
                                    keyRaw: "name"
                                },

                                description: {
                                    type: "String",
                                    keyRaw: "description"
                                },

                                category: {
                                    type: "Category",
                                    keyRaw: "category",

                                    fields: {
                                        slug: {
                                            type: "String",
                                            keyRaw: "slug"
                                        },

                                        name: {
                                            type: "String",
                                            keyRaw: "name"
                                        },

                                        id: {
                                            type: "ID",
                                            keyRaw: "id"
                                        }
                                    }
                                },

                                minimalVariantPrice: {
                                    type: "Money",
                                    keyRaw: "minimalVariantPrice",

                                    fields: {
                                        currency: {
                                            type: "String",
                                            keyRaw: "currency"
                                        },

                                        amount: {
                                            type: "Float",
                                            keyRaw: "amount"
                                        }
                                    }
                                },

                                slug: {
                                    type: "String",
                                    keyRaw: "slug"
                                },

                                images: {
                                    type: "ProductImage",
                                    keyRaw: "images",

                                    fields: {
                                        id: {
                                            type: "ID",
                                            keyRaw: "id"
                                        },

                                        alt: {
                                            type: "String",
                                            keyRaw: "alt"
                                        },

                                        url: {
                                            type: "String",
                                            keyRaw: "url"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    },

    policy: "NetworkOnly"
};