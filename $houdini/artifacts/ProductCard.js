export default {
    name: "ProductCard",
    kind: "HoudiniFragment",
    hash: "e27fb5febbeb8a40226bb8daadafbf828c2e1840044087a1c877fa7c2e3595eb",

    raw: `fragment ProductCard on Product {
  name
  description
  category {
    slug
    name
    id
  }
  minimalVariantPrice {
    currency
    amount
  }
  slug
  images {
    id
    alt
    url
  }
}
`,

    rootType: "Product",

    selection: {
        name: {
            type: "String",
            keyRaw: "name"
        },

        description: {
            type: "String",
            keyRaw: "description"
        },

        category: {
            type: "Category",
            keyRaw: "category",

            fields: {
                slug: {
                    type: "String",
                    keyRaw: "slug"
                },

                name: {
                    type: "String",
                    keyRaw: "name"
                },

                id: {
                    type: "ID",
                    keyRaw: "id"
                }
            }
        },

        minimalVariantPrice: {
            type: "Money",
            keyRaw: "minimalVariantPrice",

            fields: {
                currency: {
                    type: "String",
                    keyRaw: "currency"
                },

                amount: {
                    type: "Float",
                    keyRaw: "amount"
                }
            }
        },

        slug: {
            type: "String",
            keyRaw: "slug"
        },

        images: {
            type: "ProductImage",
            keyRaw: "images",

            fields: {
                id: {
                    type: "ID",
                    keyRaw: "id"
                },

                alt: {
                    type: "String",
                    keyRaw: "alt"
                },

                url: {
                    type: "String",
                    keyRaw: "url"
                }
            }
        }
    }
};