export default {
    name: "SomeProducts",
    kind: "HoudiniQuery",
    hash: "4044e6094846216e37567c09a18310c3fd5e5c53882c34cf50cce18cd32313bf",

    raw: `query SomeProducts {
  products(first: 24) {
    edges {
      node {
        id
        name
        description
        category {
          slug
          name
          id
        }
        minimalVariantPrice {
          currency
          amount
        }
        slug
        images {
          id
          alt
          url
        }
      }
    }
  }
}
`,

    rootType: "Query",

    selection: {
        products: {
            type: "ProductCountableConnection",
            keyRaw: "products(first: 24)",

            fields: {
                edges: {
                    type: "ProductCountableEdge",
                    keyRaw: "edges",

                    fields: {
                        node: {
                            type: "Product",
                            keyRaw: "node",

                            fields: {
                                id: {
                                    type: "ID",
                                    keyRaw: "id"
                                },

                                name: {
                                    type: "String",
                                    keyRaw: "name"
                                },

                                description: {
                                    type: "String",
                                    keyRaw: "description"
                                },

                                category: {
                                    type: "Category",
                                    keyRaw: "category",

                                    fields: {
                                        slug: {
                                            type: "String",
                                            keyRaw: "slug"
                                        },

                                        name: {
                                            type: "String",
                                            keyRaw: "name"
                                        },

                                        id: {
                                            type: "ID",
                                            keyRaw: "id"
                                        }
                                    }
                                },

                                minimalVariantPrice: {
                                    type: "Money",
                                    keyRaw: "minimalVariantPrice",

                                    fields: {
                                        currency: {
                                            type: "String",
                                            keyRaw: "currency"
                                        },

                                        amount: {
                                            type: "Float",
                                            keyRaw: "amount"
                                        }
                                    }
                                },

                                slug: {
                                    type: "String",
                                    keyRaw: "slug"
                                },

                                images: {
                                    type: "ProductImage",
                                    keyRaw: "images",

                                    fields: {
                                        id: {
                                            type: "ID",
                                            keyRaw: "id"
                                        },

                                        alt: {
                                            type: "String",
                                            keyRaw: "alt"
                                        },

                                        url: {
                                            type: "String",
                                            keyRaw: "url"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    },

    policy: "NetworkOnly"
};