export type ProductCard = {
    readonly "shape"?: ProductCard$data,
    readonly "$fragments": {
        "ProductCard": true
    }
};

export type ProductCard$data = {
    readonly name: string,
    readonly description: string,
    readonly category: {
        readonly slug: string,
        readonly name: string
    } | null,
    readonly minimalVariantPrice: {
        readonly currency: string,
        readonly amount: number
    } | null,
    readonly slug: string,
    readonly images: ({
        readonly id: string,
        readonly alt: string,
        readonly url: string
    } | null)[] | null
};