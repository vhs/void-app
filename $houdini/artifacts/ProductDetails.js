export default {
    name: "ProductDetails",
    kind: "HoudiniQuery",
    hash: "4f50701801d4656ce85b806077576133fdb5eeb384ea99edb5bebd393622c784",

    raw: `query ProductDetails($slug: String) {
  product(slug: $slug) {
    name
    description
    images {
      url
      alt
      sortOrder
      id
    }
    minimalVariantPrice {
      currency
      amount
    }
    id
  }
}
`,

    rootType: "Query",

    selection: {
        product: {
            type: "Product",
            keyRaw: "product(slug: $slug)",

            fields: {
                name: {
                    type: "String",
                    keyRaw: "name"
                },

                description: {
                    type: "String",
                    keyRaw: "description"
                },

                images: {
                    type: "ProductImage",
                    keyRaw: "images",

                    fields: {
                        url: {
                            type: "String",
                            keyRaw: "url"
                        },

                        alt: {
                            type: "String",
                            keyRaw: "alt"
                        },

                        sortOrder: {
                            type: "Int",
                            keyRaw: "sortOrder"
                        },

                        id: {
                            type: "ID",
                            keyRaw: "id"
                        }
                    }
                },

                minimalVariantPrice: {
                    type: "Money",
                    keyRaw: "minimalVariantPrice",

                    fields: {
                        currency: {
                            type: "String",
                            keyRaw: "currency"
                        },

                        amount: {
                            type: "Float",
                            keyRaw: "amount"
                        }
                    }
                },

                id: {
                    type: "ID",
                    keyRaw: "id"
                }
            }
        }
    },

    input: {
        fields: {
            slug: "String"
        },

        types: {}
    },

    policy: "NetworkOnly"
};