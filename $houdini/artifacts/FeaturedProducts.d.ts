export type FeaturedProducts = {
    readonly "input": null,
    readonly "result": FeaturedProducts$result
};

export type FeaturedProducts$result = {
    readonly products: {
        readonly edges: ({
            readonly node: {
                readonly id: string,
                readonly $fragments: {
                    ProductCard: true
                }
            }
        })[]
    } | null
};