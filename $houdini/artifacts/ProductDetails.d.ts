export type ProductDetails = {
    readonly "input": ProductDetails$input,
    readonly "result": ProductDetails$result
};

export type ProductDetails$result = {
    readonly product: {
        readonly name: string,
        readonly description: string,
        readonly images: ({
            readonly url: string,
            readonly alt: string,
            readonly sortOrder: number | null
        } | null)[] | null,
        readonly minimalVariantPrice: {
            readonly currency: string,
            readonly amount: number
        } | null
    } | null
};

export type ProductDetails$input = {
    slug: string | null | undefined
};