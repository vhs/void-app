export default {
    name: "ShopName",
    kind: "HoudiniQuery",
    hash: "3ee42cd4abb5be32339102ab00933595995d2c88dfad64f422c2257803e2f4c1",

    raw: `query ShopName {
  shop {
    name
  }
}
`,

    rootType: "Query",

    selection: {
        shop: {
            type: "Shop",
            keyRaw: "shop",

            fields: {
                name: {
                    type: "String",
                    keyRaw: "name"
                }
            }
        }
    },

    policy: "NetworkOnly"
};