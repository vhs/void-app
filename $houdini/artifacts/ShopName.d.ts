export type ShopName = {
    readonly "input": null,
    readonly "result": ShopName$result
};

export type ShopName$result = {
    readonly shop: {
        readonly name: string
    }
};