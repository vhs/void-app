export type SomeProducts = {
    readonly "input": null,
    readonly "result": SomeProducts$result
};

export type SomeProducts$result = {
    readonly products: {
        readonly edges: ({
            readonly node: {
                readonly id: string,
                readonly name: string,
                readonly description: string,
                readonly category: {
                    readonly slug: string,
                    readonly name: string
                } | null,
                readonly minimalVariantPrice: {
                    readonly currency: string,
                    readonly amount: number
                } | null,
                readonly slug: string,
                readonly images: ({
                    readonly id: string,
                    readonly alt: string,
                    readonly url: string
                } | null)[] | null
            }
        })[]
    } | null
};