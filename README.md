# Void App

![status-badge](https://ci.codeberg.org/api/badges/vhs/void-app/status.svg)

Storefront for [Saleor Commerce platform](https://saleor.io/).

## Developing

Once you've copied the project and installed dependencies with `pnpm install`, start a development server:

```bash
pnpm dev

# or start the server and open the app in a new browser tab
pnpm dev -- --open
```

Application expects the Saleor Commerce GraphQL API to be running on port 8000 on the development machine. The API can be stood up and the backing database seeded with test data using [Saleor Platform](https://github.com/saleor/saleor-platform). Once active `cp .env.example .env` and restart the development server.

## Building

Before creating a production version of your app, install an [adapter](https://kit.svelte.dev/docs#adapters) for your target environment. Then:

```bash
pnpm build
```

> You can preview the built app with `pnpm preview`, regardless of whether you installed an adapter. This should _not_ be used to serve your app in production.

## Rights

Unless otherwise noted all source code is made available under the AGPL-3.0 or a compatible license. See the file COPYING in the source for more information on your rights. SVG icon vectors are [heroicons](https://github.com/tailwindlabs/heroicons) and are MIT-licensed.
