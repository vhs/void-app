import toml from 'toml';

import rehypeSlug from 'rehype-slug';
import rehypeAutolinkHeadings from 'rehype-autolink-headings';

export default {
	extensions: ['.svx', '.md'],
	frontmatter: {
		parse(frontmatter, messages) {
			try {
				return toml.parse(frontmatter);
			} catch ({ line, column, message }) {
				messages.push(`Parsing error on line ${line}, column ${column} : ${message}`);
			}
		},
		type: 'toml',
		marker: '+'
	},
	rehypePlugins: [
		rehypeSlug,
		[
			rehypeAutolinkHeadings,
			{
				properties: {
					ariaHidden: true,
					tabIndex: -1,
					class: 'text-size-collapse'
				},
				test: ['h2', 'h3'],
				behavior: 'append',
				content: {
					type: 'element',
					tagName: 'span',
					properties: {
						class: ['material-icons text-black dark:text-white']
					},
					children: [
						{
							type: 'text',
							value: 'bookmark_border'
						}
					]
				}
			}
		]
	]
};
