import adapter from '@sveltejs/adapter-auto';
import sveltePreprocess from 'svelte-preprocess';
import { mdsvex } from 'mdsvex';
import mdsvexConfig from './mdsvex.config.js';
import precompileIntl from 'svelte-intl-precompile/sveltekit-plugin';
import houdini from 'houdini-preprocess';
import path from 'path';
import icons from 'unplugin-icons/vite';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	extensions: ['.svelte', ...mdsvexConfig.extensions],
	preprocess: [
		sveltePreprocess({
			postcss: true
		}),
		mdsvex({
			...mdsvexConfig,
			layout: {
				AboutPage: './src/layouts/AboutPage.svelte'
			}
		}),
		houdini()
	],

	kit: {
		adapter: adapter(),
		csp: {
			mode: 'auto',
			directives: {
				'script-src': ['self']
			}
		},
		vite: {
			plugins: [
				precompileIntl('locales'), // if your translations are defined in /locales/[lang].json
				icons({
					compiler: 'svelte'
				})
			],
			resolve: {
				alias: {
					$houdini: path.resolve('.', '$houdini'),
					$components: path.resolve('.', 'src/components'),
					$layouts: path.resolve('.', 'src/layouts')
				}
			},
			server: {
				fs: {
					allow: ['.'] // give houdini access to project directory
				}
			},
			envPrefix: 'VOID'
		}
	}
};

export default config;
