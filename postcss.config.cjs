const tailwindcss = require('tailwindcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');

const prod = process.env.NODE_ENV === 'production';

const config = {
	plugins: [
		tailwindcss(),
		autoprefixer,
		prod &&
			cssnano({
				preset: ['default']
			})
	]
};

module.exports = config;
