import toml from 'toml';

const config = `
baseUrl = "https://void-app-vhs.vercel.app"
languageCode = "en-US"
availableLanguages = ['en', 'id']
title = "Void App"
[params]
description = "Saleor meets Svelte"
[[params.images]]
url = "https://vhs.codeberg.page/images/8pd8ycjjkiq-maxime-le-conte-des-floris.jpg"
[[menu.main]]
name = "Home"
weight = 1
identifier = 'home'
url = '/'
[[menu.main]]
name = "About"
weight = 2
identifier = 'about'
url = '/about'
`;

interface OpenGraphImage {
	url: string;
	width?: number;
	height?: number;
	alt?: string;
}

interface ConfigProps {
	baseUrl: string;
	languageCode: string;
	availableLanguages: string[];
	title: string;
	params: { description: string; images: OpenGraphImage[] };
	menu: {
		[x: string]: {
			name: string;
			weight: number;
			identifier: string;
			url: string;
		}[];
	};
}

const parsed: ConfigProps = toml.parse(config);

export default parsed;
