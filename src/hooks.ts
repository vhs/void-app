import crypto from 'crypto';
import cookie from 'cookie';
import alp from 'accept-language-parser';
import type { GetSession, Handle, ExternalFetch } from '@sveltejs/kit';
import { sequence } from '@sveltejs/kit/hooks';
import config from './config.toml';

import type * as minify from '@minify-html/js';
import { createRequire } from 'module';
const require = createRequire(import.meta.url);
const mf = require('@minify-html/js') as typeof minify;

const prod = process.env.NODE_ENV === 'production';
interface Serializable {
	[x: string]: string;
}

const getUserInformation = (parsed: Serializable) => {
	const userId = parsed.userId || crypto.randomUUID();
	return { ...parsed, userId };
};

export const handle: Handle = sequence(
	async ({ event, resolve }) => {
		return await resolve(event, {
			ssr: !event.url.pathname.startsWith('/admin')
		});
	},
	async ({ event, resolve }) => {
		const response = await resolve(event);
		const body = await response.text();
		return new Response(
			body.replace('<html lang="%void.lang%">', `<html lang="${config.languageCode}">`),
			response
		);
	},
	async ({ event, resolve }) => {
		const response = await resolve(event);
		const header = response.headers.get('content-type');
		if (prod && header === 'text/html') {
			const config = mf.createConfiguration({
				minify_js: false,
				minify_css: false
			});
			let body = await response.text();
			body = mf.minify(body, config).toString();
			return new Response(body, response);
		}
		return response;
	},
	async ({ event, resolve }) => {
		const header = event.request.headers.get('cookie');
		const parsed = cookie.parse(header ?? '');
		event.locals.user = getUserInformation(parsed);
		const response = await resolve(event);
		if (!parsed.userId) {
			response.headers.set(
				'set-cookie',
				cookie.serialize('userId', event.locals.user.userId, {
					path: '/',
					httpOnly: true
				})
			);
		}
		return response;
	}
);

interface SessionData {
	locale: string;
}

export const getSession: GetSession = (event): SessionData => {
	const header = event.request.headers.get('accept-language');
	return config.languageCode
		? { locale: config.languageCode }
		: {
				locale: alp.pick(config.availableLanguages, header)
		  };
};

export const externalFetch: ExternalFetch = async (request) => {
	if (request.url.startsWith(`${import.meta.env.VOID_EXTERNAL_GRAPHQL_URL}`)) {
		const json = await request.json();
		request = new Request(
			request.url.replace(
				`${import.meta.env.VOID_EXTERNAL_GRAPHQL_URL}`,
				`${import.meta.env.VOID_INTERNAL_GRAPHQL_URL}`
			),
			{ method: request.method, headers: request.headers, body: JSON.stringify({ ...json }) }
		);
	}

	return fetch(request);
};
