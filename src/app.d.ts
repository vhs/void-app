/// <reference types="@sveltejs/kit" />
/// <reference types="vite/client" />
/// <reference types="unplugin-icons/types/svelte" />
/// <reference types="mdsvex/globals" />

declare module '$locales/*';
interface ImportMetaEnv {
	readonly VOID_INTERNAL_GRAPHQL_URL: string;
	readonly VOID_EXTERNAL_GRAPHQL_URL: string;
}

interface ImportMeta {
	readonly env: ImportMetaEnv;
}

// See https://kit.svelte.dev/docs#typescript
// for information about these interfaces
declare namespace App {
	interface Locals {
		user: {
			userId: string;
		};
	}

	interface Platform {}

	interface Session {}

	interface Stuff {}
}
