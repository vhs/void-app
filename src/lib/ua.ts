/**
 * Save a preference in local storage.
 * @param key {string} persistence identifier
 * @param value {string} string blob
 */
export const savePreference = (key: string, value: string) => {
	localStorage.setItem(key, value);
};

/**
 * Get a saved preference from local storage.
 * @param key {string} persistence identifier
 * @returns string blob
 */
export const getSavedPreference = (key: string) => {
	return localStorage.getItem(key);
};
