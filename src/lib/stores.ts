import { writable, readable } from 'svelte/store';

// Custom store to display page schema for markdown pages.
export const shouldShowPageSchema = (() => {
	const { subscribe, set, update } = writable(false);

	return {
		subscribe,
		toggle: () => update((toggled) => !toggled),
		reset: () => set(false)
	};
})();

// TODO: Provide access to configure via config.toml
export const currencyCode = writable('USD');

// Handle user preference to avoid vestibular motion triggers.
// TODO: Test and integrate with support for SSR
export const prefersReducedMotion = (() => {
	const { subscribe } = readable(
		() => window.matchMedia('(prefers-reduced-motion: reduce)').matches
	);

	return { subscribe };
})();
