import config from '../config.toml';

export const classes = (...classes: string[]) => classes.filter(Boolean).join(' ');

export const formatCurrency = (
	price: string,
	locale?: string | undefined,
	currencyCode?: string
) => {
	return Intl.NumberFormat(locale, {
		style: 'currency',
		currency: currencyCode ? currencyCode : 'USD',
		minimumFractionDigits: 2
	}).format(parseFloat(price ? price : '0'));
};

export const formatTitle = (str: string) => {
	const replacer = (match: string, nondigits: any) => {
		return (nondigits ? ' ' : '') + match.toUpperCase();
	};
	return str.replace(/^[a-z]|[A-Z]/g, replacer);
};

export const formatDateIntl = (date: string | number | Date) => {
	return new Intl.DateTimeFormat(config.languageCode, {
		dateStyle: 'full',
		timeStyle: 'long'
	}).format(new Date(date));
};
