#
# Copyright (C) 2022  VHS <vhsdev@tutanota.com>
#
# This file is part of Void App.
#
# Void App is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Void App is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# DOCKER-VERSION 20.10.12, build e91ed5707e

# Get a Node container and cautiously pin version.
FROM node:lts-alpine@sha256:2f50f4a428f8b5280817c9d4d896dbee03f072e93f4e0c70b90cc84bd1fcfe0d

# Install package manager for Node.
WORKDIR /tmp
RUN apk add gpg gpg-agent && \
  wget -qO - https://keybase.io/pnpm/pgp_keys.asc | gpg --import && \
  wget -q https://get.pnpm.io/SHASUMS256.txt && \
  wget -q https://get.pnpm.io/SHASUMS256.txt.sig && \
  gpg --verify SHASUMS256.txt.sig SHASUMS256.txt && \
  wget https://get.pnpm.io/v6.16.js && \
  grep v6.16.js SHASUMS256.txt | sha256sum -c - && \
  cat v6.16.js | node - add --global pnpm && \
  rm SHASUMS256.txt v6.16.js

# Get Node dependencies from lockfile.
WORKDIR /app
COPY pnpm-lock.yaml ./
RUN pnpm fetch --dev

# Add app source and install Node deps offline.
ADD . ./
RUN pnpm install --offline --dev

# Build application for production.
RUN pnpm run build

# Expose container port 3000 when requested.
EXPOSE 3000

# Preview production build by default.
CMD [ "pnpm", "preview", "--", "--port", "3000", "--host" ]